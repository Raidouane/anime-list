﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimeList.models;
using Xamarin.Forms;

namespace AnimeList
{
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<Anime> _myAnimeList;

        public MainPage()
        {
            InitializeComponent();

            PopulateAnimeList();
        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            if (!(sender is MenuItem item) || item.CommandParameter == null)
            {
                return;
            }

            RemoveItemById(item.CommandParameter.ToString());
        }

        void HandleCellTapped(object sender, System.EventArgs e)
        {
            if (!(sender is ViewCell item) || item.BindingContext == null)
            {
                return;
            }

            var anime = item.BindingContext as Anime;
            RemoveItemById(anime.ID);
        }

        void HandleRefreshing(object sender, System.EventArgs e)
        {
            if (_myAnimeList.Count < 5)
            {
                PopulateAnimeList();
            }

            AnimeListView.IsRefreshing = false;
        }

        private void RemoveItemById(string id)
        {
            Anime item = (from itm in _myAnimeList where itm.ID == id select itm)
                             .FirstOrDefault<Anime>();
            _myAnimeList.Remove(item);
        }

        private void PopulateAnimeList()
        {
            _myAnimeList = new ObservableCollection<Anime>();
            var onePiece = new Anime()
            {
                ID = "0",
                Name = "One Piece",
                ReleaseYear = 1997,
                EpisodesNumber = 872,
                ImagePath = "op.png",
            };
            var naruto = new Anime()
            {
                ID = "1",
                Name = "Naruto",
                ReleaseYear = 2002,
                EpisodesNumber = 720,
                ImagePath = "naruto.jpg",
            };
            var madeInAbyss = new Anime()
            {
                ID = "2",
                Name = "Made in Abyss",
                ReleaseYear = 2017,
                EpisodesNumber = 13,
                ImagePath = "mia.jpg",
            };
            var drr = new Anime()
            {
                ID = "3",
                Name = "Durarara",
                ReleaseYear = 2010,
                EpisodesNumber = 36,
                ImagePath = "drr.jpg",
            };
            var tpn = new Anime()
            {
                ID = "4",
                Name = "The Promised Neverland",
                ReleaseYear = 2019,
                EpisodesNumber = 24,
                ImagePath = "tpn.png",
            };

            _myAnimeList.Add(onePiece);
            _myAnimeList.Add(naruto);
            _myAnimeList.Add(madeInAbyss);
            _myAnimeList.Add(drr);
            _myAnimeList.Add(tpn);
            AnimeListView.ItemsSource = _myAnimeList;
        }
    }
}
