﻿using System;
namespace AnimeList.models
{
    public class Anime
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public uint ReleaseYear
        {
            get;
            set;
        }

        public uint EpisodesNumber
        {
            get;
            set;
        }

        public string ImagePath
        {
            get;
            set;
        }
    }
}
